Suru
====

A PSR complient easy and fast PHP Project Skeleton


##Features

- Full Featured Application Skeleton
- Authentication
- Authorization ( Multiple Role Based )
- View ( Twig)
- Controller ( Slim)
- Model ( Paris | Idiorm)
- Fully Featured Admin Control Panel
- Individual Profile
- JSON Management
- Session Management
- Out-of-Box CRUD System
- more coming soon
